# HTCondor Startd deployment at CERN

This GitOps configuration aims to deploy a HTCondor Startd instance at CERN relying on `IDTOKENS`, thus mimimising the requirements on other entities.

Compared to previous approaches, this implies:

* Not relying on HashiCorp Consul for discovery.
* Use `IDTOKENS` to authenticate the new startd.

## Deployment workflow (_draft_)

1. A new Kubernetes cluster is deployed at CERN.
2. The Kubernetes cluster nodes are listed and their IPs used to invoke `condor_token_auto_approve` for a period of time (`1h`).
3. When this repository is deployed, HTCondor will try to fetch an `IDTOKEN` and thanks to the rules created in the previous step it will be provided by the Collector.
4. Once the token is available, HTCondor Startd will start and no further additions to the map file will be needed as the identity is provided on the token.

## GitOps structure

The initial plan is to define one release per target cluster:

* `test` release connects the startds to our test cluster.
* `prod` release connects the startds to our _share_ production cluster.
